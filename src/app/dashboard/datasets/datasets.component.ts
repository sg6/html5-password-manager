import {Component, ElementRef, OnInit, Renderer, ViewChild} from '@angular/core';
import { DatasetService } from './datasets.service';
import { ActivatedRoute, Params} from '@angular/router';


@Component({
  selector: 'app-datasets',
  templateUrl: './datasets.component.html',
  styleUrls: ['./datasets.component.scss'],
  providers: [DatasetService]
})
export class DatasetsComponent implements OnInit {

  items = [];
  id;
  lastKeyPress : String;
  selectedDataset : number;
  message : String = "";
  activePassword : String = "";
  createNewDataset : Boolean = false;

  constructor(private _datasetsService: DatasetService, private activatedRoute: ActivatedRoute, private renderer: Renderer) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.selectedDataset = 0;
      this.id = params['id'];
      this.getDatasets();
    });
  }

  selectDataset(e:MouseEvent, i: any) : void {
      this.selectedDataset = i;
  }

  getDatasets() {
    this._datasetsService.getItems(this.id).subscribe(data => {
      this.items = data;
    });
  }

  onShow(val: boolean) {
    this.createNewDataset = val;
    this.getDatasets();
  }

  onKeyDown($event) : void {
    if((this.lastKeyPress === "c" && $event.key === "Control") || (this.lastKeyPress === "Control" && $event.key === "c")) {
        if(this.items[this.selectedDataset]._id !== undefined) {
            this._datasetsService.getPassword(this.items[this.selectedDataset]._id)
                .subscribe(data => {
                    this.activePassword = data.password;
                });
        }
    }
    if($event.key === "ArrowUp") {
        if(this.selectedDataset > 0) this.selectedDataset--;
        else this.selectedDataset = this.items.length - 1;
    } else if($event.key === "ArrowDown") {
        if(this.selectedDataset < (this.items.length - 1)) this.selectedDataset++;
        else this.selectedDataset = 0;
    }

    this.lastKeyPress = $event.key;
  }

  @ViewChild('passInput') input: ElementRef;
  onKeyUp($event) : void {

    if((this.lastKeyPress === "c" && $event.key === "Control") || (this.lastKeyPress === "Control" && $event.key === "c")) {

      this.renderer.invokeElementMethod(this.input.nativeElement, 'select');
      document.execCommand('copy');
      this.message = `Pass of ${this.items[this.selectedDataset].title} copied`;
    }

  }

  toggleNewDataset() : void {
      this.createNewDataset = !this.createNewDataset;
  }

}
