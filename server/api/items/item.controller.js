'use strict';

var Item = require('./item.model');
var auth = require('../../auth/auth.service');
var itemService = require('./item.service');
var passport = require('passport');
var Category = require('../categories/category.model');
var ObjectId = require('mongoose').Types.ObjectId;


exports.create = function (req, res) {
    var item = {
        title: req.body.title,
        username: req.body.username,
        password: req.body.password,
        description: req.body.description,
        URL: req.body.url,
        note: req.body.note,
        expire_date: req.body['expire_date'],
        //policy: JSON.parse(req.body.policy),
        attributes: []
    };
    var newItem = new Item(item);
    newItem.save(function (err, doc) {
        if (err) return err;
        Category.findByIdAndUpdate( req.body.category, { $push : { "items" : doc._id } }, {safe: true, upsert: true}, function(err, cat) {
            if(err) return err;
            console.log(cat);
        } )
        res.json(doc);
    });
};


// index Item with get method to get all Items
exports.index = function (req, res) {

    if(req.query.category) {
        Category.findById(req.query.category, {}, function(err, doc) {
            if(err || !doc) {
                res.json([]);
                return err;
            }
            if(!doc) {
                res.json([]);
                return;
            }
            if(doc.items.length > 0) {
                Item.find({ "_id" : { $in : doc.items} }, { password : 0 }, function(err, items) {
                   res.json(items);
                });
            } else {
                res.json([]);
            }
        });
    } else {
        Item.find({}, { password: 0 }, function (err, items) {
            console.log(123);
            res.json(items);
        });
    }
};


// find one specific Item
exports.findOne = function (req, res) {
    // if(req.user === undefined) res.status(401);
    var id = req.params.id;
    if(req.query.value === "password") {
        Item.findById(req.params.id, { password: 1 }, function (err, doc) {
            res.json(doc);
        });
    } else if(req.query.value === "all") {
        Item.findById(req.params.id, function (err, doc) {
            res.json(doc);
        });
    } else {
        Item.findById(req.params.id, { password: 0 }, function (err, doc) {
            res.json(doc);
        });
    }
};


// update particular id
exports.update = function (req, res) {
    var item = {
        username: req.body.username,
        name: req.body.name,
        password: req.body.password,
        URL: req.body.URL,
        category_id: req.body.category_id
    };

    var conditions = { _id: req.params.id }
        , update = item
        , options = { multi: true };

    Item.update(conditions, update, options, callback);

    function callback(err, numAffected) {
        // numAffected is the number of updated documents
        res.json(numAffected);
    }

};


exports.delete = function (req, res) {
    if(Item.findOne({ _id: req.params.id }, function (err, doc) {
        if (err) return console.log(err);
        doc.remove();
        res.json({ removed: doc });
    })) { }
    else res.json({ removed: 0 });
}
