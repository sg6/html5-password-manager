# Password Manager - HTML5 Application

This is the web application **Password Manager**.
It uses the MEAN stack which consists of **M**ongoDB, **E**xpressJS, **A**ngular, **N**odeJS.
The server platform is NodeJS where the whole application is based on.
Angular is the client side framework and is used for the single-page JavaScript application and as a tempate engine, as well.
MongoDB is the NoSQL database to store the data in.
ExpressJS is the server side framework which is used for the REST api, routing, authentication, etc.

For the frontend, Sass is used to create dynamic CSS files.
Grunt is used as a watcher for file changes.

# Documentation

Docs can be found [here](docs/index.md)

# Deployment

Run `ng build`, then `node server` and the application will run on [localhost:3100](http://localhost:3100). 

# What you need

The following applications are needed for building and running the application. The following installation "guides" are meant for Ubuntu 14.04.

## NodeJS, NPM

```
curl -sL https://deb.nodesource.com/setup | sudo bash -
sudo apt-get install nodejs
sudo apt-get install build-essential
```

## MongoDB

```
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo "deb http://repo.mongodb.org/apt/ubuntu "$(lsb_release -sc)"/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list
sudo apt-get update
sudo apt-get install -y mongodb-org
```
## Grunt

```
sudo npm install -g grunt 
sudo npm install -g grunt-cli
```

## Git

```
sudo apt-get install git
```

## Angular CLI

`sudo npm install -g @angular/cli`

# How to start?

When all applications are installed on the development computer, you can start coding.

## Fetch the files

Fetch the files from the repository, use the ```git clone``` command to copy the data to your device.

Copy the file `.env.example.ts` to `.env.ts` and insert the URL of the API (most probably `http://localhost:3100`).

## Build files and start coding

For the first start, execute ``npm install`` - all required files will be downloaded from the web assigned repositories.

Run `grunt ` to start the backend application, with live-reloading when you change server-relevant code.

Run `ng serve` to start coding the frontend.

# Angular CLI

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0-rc.2.

## Development server

Run `ng serve` for a dev server. 
Navigate to `http://localhost:4200/`. 
The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. 
The build artifacts will be stored in the `dist/` directory. 
Use the `-prod` flag for a production build.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

