# Application Architecture

The application consists of a the backend application, writte in Node.js, which is connected to a MongoDB and to the frontend, written in Angular 2.

![Application Architecture](application-architecture.png)

This document is a general description of the application.
To get details about the code, please refer to the [Code Documentation](./code.md).

## Backend

The backend application is based on components.
Each component consists of a model, which is a Mongoose model and creates a bridge to the database.
The application logic of each component is written in the controller.
Communication between frontend and backend is handled over a REST web service, where a REST endpoint is directly connected to one controller's method.


### Services

There are 2 types of services:

1. Components' service: a service of a component handles complex logic for a controller and can only be used within one component.
1. Global services: these can be used through the whole application


### Components

![Backend Architecture](./backend-architecture.png)

There are 6 main components, which are connected to each other.
Additionally, there is a log component -- its only task is to save interaction between the user and the application.

#### Item

The core component of the application is the Item component.
An item is a dataset, where the password is being saved encrypted.

An item consists of the following fields:

* _id
* title
* username
* password
* description
* URL
* expire_date
* note
* policy [*]
* attributes{} [**]
* access_allowed: { roles[], users[] } [***]
* access_denied: { roles[], users[] } [***]

[*] holds the _id of a policy component

[**] holds an optional number of attributes, which can be used in the frontend (e.g. colour)

[***] holds an array of _ids from roles and users


#### Categories

A dataset is always assigned to one category.
A category can be a main category, or a sub-category of another category.
For performance reasons, a category stores an array of items.
Additionally to a name, a category has an alias, which is a URL-friendly and unique value to recognize the category easily.

A category consists of the following fields:

* _id
* name
* alias
* parent (optional)
* items[]
* attributes{}
* access_allowed: { roles[], users[] }
* access_denied: { roles[], users[] }


#### Tags

To find similar items of different categories, tags are used within this applications.
They are similar to categories, but easier; an item can contain multiple tags, and tags have no hierarchy.

A tag consists of the following fields:

* _id
* name
* alias
* items[]



#### Policies

A policy, shorthand for password-policy, is a set of rules which can be assigned to an item.

A policy is a guideline for a dataset's password and consists of the following fields:

* _id
* name
* rules: { min-length, max-length, lowercase, uppercase, numbers, special-characters } [*]

[*] The keys min-length and max-length are numbers, all other keys can contain one of the following values: required, optional, not-allowed



#### Users

A user can log in into the application and is can be assigned to multiple roles.

A user consists of the following fields:

* _id
* email
* password
* roles[]
* attributes[]


#### Roles

A role consts of the following fields:

* _id
* name
* hierarchy [*]

[*] An optional, numeric value (higher means more important)