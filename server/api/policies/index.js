'use strict';

const express = require('express');
const controller = require('./policies.controller.js');
const router = express.Router();


router.post('/', controller.create);
router.get('/', controller.index);
router.get('/:id', controller.findOne);
router.get('/:id/passwords', controller.findPasswords);
router.put('/:id', controller.update);

module.exports = router;
