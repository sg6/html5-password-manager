'use strict';

var User = require('./user.model');
var userService = require('./user.service');


exports.create = function(req, res) {
    var user = {
        username : req.body.username,
        email : req.body.email,
        password : req.body.password,
        name : req.body.name,
    };
    var newUser = new User(user);
    newUser.save(function(err, doc) {
        if (err) return err;
        res.json(doc);
    });
};

exports.index = function(req, res) {
    // get all users

    User.find({}, function(err, users) {

    res.json(users);  
  });


};

exports.findOne = function(req, res) {

    var id=req.params.id;
    console.log(id);

    User.findById(req.params.id, function (err, doc){

        res.json(doc); 
      // doc is a Document
    });
    // find one user by ID
};

exports.update = function(req, res) {
var user = {
        username : req.body.username,
        email : req.body.email,
        password : req.body.password,
        name : req.body.name,
    };

  var conditions = { _id: req.params.id }
  , update = user
  , options = { multi: true };

User.update(conditions, update, options, callback);

function callback (err, numAffected) {
  // numAffected is the number of updated documents
    res.json(numAffected);
}

};

exports.me = function(req, res) {
  if(req.user === undefined) {
    res.json({});
    return;
  }
  var user = {
    email : req.user.email,
    name : req.user.name,
    username : req.user.username
  };
  res.json(user);
};