import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from './auth/login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CategoriesComponent } from './dashboard/categories/categories.component';
import { DatasetsComponent } from './dashboard/datasets/datasets.component';
import { RoutingModule } from "./routing.module";
import { DatasetDetailComponent } from './dashboard/datasets/detail/detail.component';
import { CreateDatasetComponent } from './dashboard/datasets/create-dataset/create-dataset.component';
import { EditComponent } from './dashboard/categories/edit/edit.component';
import { CategoryTreeComponent } from './dashboard/categories/category-tree/category-tree.component';
import { SettingsComponent } from './settings/settings.component';
import { UsersComponent } from './settings/users/users.component';
import { PoliciesComponent } from './settings/policies/policies.component';
import { RolesComponent } from './settings/roles/roles.component';
import { NavComponent } from './settings/nav/nav.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    LoginComponent,
    DashboardComponent,
    CategoriesComponent,
    DatasetsComponent,
    DatasetDetailComponent,
    CreateDatasetComponent,
    EditComponent,
    CategoryTreeComponent,
    SettingsComponent,
    UsersComponent,
    PoliciesComponent,
    RolesComponent,
    NavComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
