const
    express = require('express'),
    bodyParser = require('body-parser'),
    app = express(),
    session = require('express-session'),
    passport = require('passport'),
    middleware = require('./server/middleware'),
    bcrypt = require('bcrypt'),
    cors = require('cors');

app.use(bodyParser.json());
app.use(express.static(__dirname + '/dist'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

app.use(session({
  secret : 'mysecret',
  saveUninitialized : true,
  resave: true,
  cookie : { secure : true }
}));

app.use(passport.initialize());
app.use(passport.session());

app.use('/api/users', middleware, require('./server/api/users'));
app.use('/api/items', middleware ,require('./server/api/items'));
app.use('/api/categories', middleware, require('./server/api/categories'));
app.use('/api/policies', middleware, require('./server/api/policies'));
app.use('/api/tags', middleware, require('./server/api/tags'));

app.use('/api/auth', require('./server/auth'));

app.get('/*', function(req, res) {
  res.sendFile(__dirname + "/dist/index.html");
})

const _port = 3100;
app.listen(_port);
console.log('Server running on ' + _port);    

