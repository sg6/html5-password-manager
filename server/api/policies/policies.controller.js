'use strict';

const Policies = require('./policies.model');
const policiesService = require('./policies.service');

exports.create = function(req, res) {

    let policies = {
        name        : req.body.name,
        rules       : req.body.rules
    },
        newPolicies = new Policies(policies);

    newPolicies.save(function(err, doc) {
        if (err) return err;
        res.json(doc);
    });

};


// index service with get method to get all users

exports.index = function(req, res) {


   Policies.find({}, function(err, policies) {
      res.json(policies);
    });

};


exports.findOne = function(req, res) {

  Policies.findById(req.params.id, function (err, doc){
    res.json(doc);
  });

};

exports.findPasswords = function(req, res) {

  Policies.findById(req.params.id, function (err, doc) {
    if(err || doc == null) {
      console.log(err);
      res.json({});
      return;
    }
    let rules = doc.rules;
    let pass = policiesService.passwortGenerator(
      rules['min-length'],
      rules['max-length'],
      rules.lowercase,
      rules.uppercase,
      rules.numbers,
      rules['special-characters']);
    res.json({ pass : pass });
  });

};

exports.update = function(req, res) {

  let policy = {
          name        : req.body.name,
          rules      : req.body.rules
      },
      conditions = { _id: req.params.id },
      options = { multi: true };

    Policies.update(conditions, policy, options, callback);
    function callback (err, numAffected) {
      if(err) return err;
      res.json(numAffected);
    }

};
