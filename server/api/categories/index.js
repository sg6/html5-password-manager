'use strict';

var express = require('express');
var controller = require('./category.controller.js');
var Category = require('./category.model.js');
//console.log(controller);

var router = express.Router();


router.post('/', controller.create);
router.get('/', controller.index);
router.get('/:id', controller.findOne);
router.put('/:id', controller.update);

module.exports = router;
