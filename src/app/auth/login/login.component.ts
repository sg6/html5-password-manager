import { Component, OnInit } from '@angular/core';
import { LoginService } from './login-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {

  public username : String = "stefan";
  public password : String = "password";
  constructor(public _loginService : LoginService) { }

  ngOnInit() {
  }

  login() : void {
    this._loginService.auth({username : this.username, password: this.password})
        .subscribe();
  }

}
