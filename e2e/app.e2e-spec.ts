import { Html5PasswordManagerPage } from './app.po';

describe('html5-password-manager App', () => {
  let page: Html5PasswordManagerPage;

  beforeEach(() => {
    page = new Html5PasswordManagerPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
