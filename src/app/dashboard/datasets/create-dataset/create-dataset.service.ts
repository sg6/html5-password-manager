import { Injectable } from '@angular/core';
import { env } from '../../../../../.env';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
// import { Router } from '@angular/router';
import { Http, Response } from '@angular/http';

@Injectable()
export class CreateDatasetService {

  constructor(private _http : Http) { }

  public saveItem(item) {
    return this._http
      .post(env.apiUrl + '/api/items', item)
      .map((response: Response) => {
        return response.json()
      })
  }

}
