import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-settings-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  @Input() active;
  constructor() { }

  ngOnInit() {
  }

}
