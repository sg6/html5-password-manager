import { Injectable } from '@angular/core';
import { env } from '../../../../../.env';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Router } from '@angular/router';
import { Http, Response } from '@angular/http';

@Injectable()
export class DatasetDetailService {

  constructor(private _http : Http) { }

  public getDetail(id) {
    return this._http
      .get(env.apiUrl + '/api/items/' + id + '?value=all')
      .map((response:Response) => response.json());
  }

  public saveDetail(id, data) {
    return this._http
      .put(env.apiUrl + '/api/items/' + id, data)
      .map((response: Response) => response.json());
  }

  public removeDetail(id) {
    return this._http
      .delete(env.apiUrl + '/api/items/' + id)
      .map((response: Response) => response.json())
  }


  public getPolicies() {
    return this._http
      .get(env.apiUrl + '/api/policies/')
      .map((response:Response) => response.json());
  }

  public generatePassword(id) {
    let url = `${env.apiUrl}/api/policies/${id}/passwords`;
    console.log(url);
    return this._http
      .get(url)
      .map((response:Response) => response.json());
  }
}
