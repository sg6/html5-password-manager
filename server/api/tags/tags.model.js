'use strict';


var crypto = require('crypto');

var mongoose = require('../../db_connect.js');

var Schema = mongoose.Schema;


var TagsSchema = new Schema({
  name : String,
  rules : String
});



module.exports = mongoose.model('Tags', TagsSchema);
