# Code Documentation

In this document, details about the code can be found.

## Frontend

The frontend consists of an Angular application, written with the Angular CLI.
Code, related to the frontend, can be found in the `src` directory.

To generate new components, please use `ng g c NAME_OF_COMPONENT` and refer to the official Angular CLI documentation.
Components can be also generated in sub-directories, like `ng g c categories/category-tree`.


### Routes

Routes are defined within the `routing.module.ts` file in the app folder.


### Components

* auth: the auth component consists of the login component, which handles the log in to the application
* dashboard: this is the main part of the application. It consists of dashboard and datasets.
    * A dashboard contains of the categories, which has a sub-component called category-tree. The category-tree component handles the hierarchy of the components.
    * A dataset in the frontend is equivivalent to a item in the backend. As the term "item" would have ambiguous meanings in the Angular term, it is called dataset in the frontend. It consists of the datasets component, which is a list of datasets in a category view. Additionaly, there are is a detail component and a create component. The detail component also handles the edit of a dataset.
* settings: this component handles general settings like policies, profile data and a cockpit for user management for admins.


### Services

To generate a service, run `ng g s NAME_OF_SERVICE`.
A service should be used to outsource a component's functionality, like calling http requests.


## Backend

TBD (hopefully)