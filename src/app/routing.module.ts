import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from "./dashboard/dashboard.component";
import { LoginComponent } from "./auth/login/login.component";
import { DatasetsComponent } from "./dashboard/datasets/datasets.component";
import { DatasetDetailComponent } from "./dashboard/datasets/detail/detail.component";
import { EditComponent } from "./dashboard/categories/edit/edit.component";
import { SettingsComponent } from "./settings/settings.component";
import { UsersComponent } from "./settings/users/users.component";
import { RolesComponent } from "./settings/roles/roles.component";
import { PoliciesComponent } from "./settings/policies/policies.component";

const routes: Routes = [
  { path : '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path : 'dashboard', component: DashboardComponent },
  { path : 'login', component: LoginComponent },
  { path : 'category/:id', component : DatasetsComponent },
  { path : 'category/:id/edit', component : EditComponent },
  { path : 'dataset', component : DatasetDetailComponent },
  { path : 'dataset/:id', component : DatasetDetailComponent },
  { path : 'settings', component : SettingsComponent },
  { path : 'settings/users', component : UsersComponent },
  { path : 'settings/roles', component : RolesComponent },
  { path : 'settings/policies', component : PoliciesComponent },

];

@NgModule({
    imports : [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class RoutingModule {}