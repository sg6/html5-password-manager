var passport = require('passport');
var User = require('../api/users/user.model');
var session = require('express-session')
var express = require('express');
var app = express();

function isAuthenticated(user) {
  if(user) {
    if(user !== undefined && user !== null) {
      return User.getUserById(user);
    }
  }
  return false;
}



exports.isAuthenticated = isAuthenticated;
