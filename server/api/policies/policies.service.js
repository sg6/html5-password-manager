module.exports = {
  passwortGenerator : function(min, max, lower, upper, number, special) {
    const lowers = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"];
    const uppers = lowers.map((lower) => {
      return lower.toUpperCase();
    });
    const numbers = [1,2,3,4,5,6,7,8,9,0];
    const specials = ["ö","Ö", "ä", "Ä", "ü", "Ü", ".", "-", "_", ";", ",", "\\", "/", "(", "^", ")"];
    let alphabet = [];

    if(lower === "required" || lower === "optional") alphabet = alphabet.concat(lowers);
    if(upper === "required" || upper === "optional") alphabet = alphabet.concat(uppers);
    if(number === "required" || number === "optional") alphabet = alphabet.concat(numbers);
    if(special === "required" || special === "optional") alphabet = alphabet.concat(specials);


    let adds = Math.floor(Math.random() * (max-min+1)),
        pass = [],
        ct = 0;

    if(lower === "required") {
      pass.push(lowers[Math.ceil(Math.random() * (lowers.length - 1))]);
      ct++;
    }
    if(upper === "required") {
      pass.push(uppers[Math.ceil(Math.random() * (uppers.length - 1))]);
      ct++;
    }
    if(number === "required") {
      pass.push(numbers[Math.ceil(Math.random() * (numbers.length - 1))]);
      ct++;
    }
    if(special === "required") {
      pass.push(specials[Math.ceil(Math.random() * (specials.length - 1))]);
      ct++;
    }


    for(let i = ct; i < min; i++) {
      pass.push(alphabet[Math.ceil(Math.random() * (alphabet.length - 1))]);
    }

    for(let i = 0; i < adds; i++) {
      pass.push(alphabet[Math.ceil(Math.random() * (alphabet.length - 1))]);
    }

    for(let i = 0; i < Math.floor(Math.random() * 10); i++)
    pass.sort((a,b) => {
      return 0.5 - Math.random();
    });

    let passString = "";
    pass.forEach((c) => {
      passString = passString + "" + c;
    });

    return passString;

  }
};
