'use strict';

var Category = require('./category.model');

var categoryService = require('./category.service');

exports.create = function(req, res) {
 
    var category = {
        name        : req.body.name,
        parent      : req.body.parent,
        items        : req.body.items,
        attributes  : req.body.attributes
    };
    var newCategory = new Category(category);

    newCategory.save(function(err, doc) {
        if (err) return err;
        res.json(doc);
    });
    
};


// index service with get method to get all users

exports.index = function(req, res) {
  Category.find({}, function(err, categories) {
    res.json(categories);
  });
  
};

// find one service
exports.findOne = function(req, res) {
    var id=req.params.id;
    Category.findById(req.params.id, function (err, doc){
        res.json(doc); 
    });
    
};


// update particular id 
exports.update = function(req, res) {
var category = {
        name        : req.body.name,
        parent      : req.body.parent,
        item        : req.body.item,
        attributes  : req.body.attributes
    };

  var conditions = { _id: req.params.id }
  , update = category
  , options = { multi: true };

Category.update(conditions, update, options, callback);

function callback (err, numAffected) {
  // numAffected is the number of updated documents
    res.json(numAffected);
}; 

};