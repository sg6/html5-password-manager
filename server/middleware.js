var Logs = require('./api/logs/logs.model');

function checkAuth(req, res, next) {
  if(!req.user) res.sendStatus(401);
  else next();
}

function log(req, res, next) {
  let user = (req.user) ? req.user._id : null;
  const logs = new Logs({
    url : req.originalUrl,
    method : req.method,
    ip : req.headers['x-forwarded-for'] || req.connection.remoteAddress,
    data : req.body,
    time : new Date(),
    user : user
  });
  logs.save(function(err, doc) {
    if(err) console.log(err);
    next();
  });
}


module.exports = [log];
