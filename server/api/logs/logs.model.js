'use strict';

var mongoose = require('../../db_connect.js');
var Schema = mongoose.Schema;

var LogsSchema = new Schema({
  url : String,
  method : String,
  ip : String,
  data : Object,
  time : Date,
  user : String,
});

module.exports = mongoose.model('Logs', LogsSchema);