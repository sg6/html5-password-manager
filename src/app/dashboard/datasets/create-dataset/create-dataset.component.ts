import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CreateDatasetService } from './create-dataset.service';
import { CategoryService } from '../../categories/categories.service';
import { ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-create-dataset',
  templateUrl: './create-dataset.component.html',
  styleUrls: ['./create-dataset.component.scss'],
  providers: [CreateDatasetService, CategoryService]
})
export class CreateDatasetComponent implements OnInit {

  newItem : any = {
    title : String,
    username : String,
    password : String,
    description: String,
    URL : String,
    note: String,
    expire_date : Date,
    policy : Object,
    attributes: Array
  };
  title = "test title";
  username = "testusr";
  password = "testpass";
  description = "test desc";
  URL = "http://meier.com";
  note = "this is my note";
  expire_date;
  policy;
  category;
  categories : Array<String>;
  @Output() onShow = new EventEmitter<boolean>();


  constructor(private _createDatasetService : CreateDatasetService,
    private _categoriesService : CategoryService,
    private activatedRoute : ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.category = params['id'];
    });
    this._categoriesService.getCategories().subscribe(data => {
          this.categories = data;
      });
  }

  saveNewDataset() : void {
    this.newItem = {
      title : this.title,
      username : this.username,
      password : this.password,
      description : this.description,
      URL : this.URL,
      note : this.note,
      expire_date : this.expire_date,
      policy: this.policy,
      category : this.category
    };
    this._createDatasetService.saveItem(this.newItem).subscribe((data) => {
      this.onShow.emit(false);
    })
  }


}
