var express = require('express');
var passport = require('passport');
var User = require('../api/users/user.model');
var LocalStrategy = require('passport-local').Strategy;
var _hash = require('./hash.js');

var router = express.Router();


router.post('/',
  passport.authenticate('local'),
  function(req, res) {
    var response = {
      success : 200,
      username : req.user.username,
      email : req.user.email
    };
    res.status(200).send(response);
  }
);

router.post('/master', function(req, res) {
  if(!req.user) res.sendStatus(401);
  else next();

  var _master = req.body.master || "Passwort2017";
  var _result = _hash.checkMasterKey(_master);

  if(_result) {
    req.session.master = _master;
  }

  res.json({"master" : _result });
});

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

passport.use(
  new LocalStrategy(function(username, password, done) {

    User.getAuthenticated(username, password, done);

  })
);


module.exports = router;
