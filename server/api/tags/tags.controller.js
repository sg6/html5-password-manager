'use strict';

var Tags = require('./tags.model');

var tagsService = require('./tags.service');

exports.create = function(req, res) {
 
    var tag = {
        name        : req.body.name,
        rules       : req.body.rules
    };
    var newTags = new Tags(tag);

    newTags.save(function(err, doc) {
        if (err) return err;
        res.json(doc);
    });
    
}; 


// index service with get method to get all users

exports.index = function(req, res) {
    
 
   Tags.find({}, function(err, tag) {
      res.json(tag);  
    }); 
  
};


exports.findOne = function(req, res) {

    var id=req.params.id;

    Tags.findById(req.params.id, function (err, doc){
        res.json(doc); 
    });
    
};

exports.update = function(req, res) {

  var tag = {
          name        : req.body.name,
          rules      : req.body.rules
      };
    var conditions = { _id: req.params.id }
    , update = tag
    , options = { multi: true };


    Tags.update(conditions, update, options, callback);
    function callback (err, numAffected) {
        res.json(numAffected);
    }; 

};
