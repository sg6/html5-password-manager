import { Component, OnInit } from '@angular/core';
import { DatasetDetailService } from './dataset-detail.service';
import { CategoryService } from '../../categories/categories.service';
import { ActivatedRoute, Params} from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  providers: [DatasetDetailService,CategoryService]
})
export class DatasetDetailComponent implements OnInit {

  constructor(private _datasetDetailService : DatasetDetailService,
    private activatedRoute : ActivatedRoute,
    private _categoriesService : CategoryService,
    private _location: Location) { }

  id;
  title;
  username;
  password ;
  description;
  URL;
  note;
  expire_date;
  policy;
  policies;
  category;
  isAwaiting = false;
  categories : Array<String>;
  msg;


  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
        let _id = params['id'];
        this._datasetDetailService.getDetail(_id).subscribe((data) => {
          this.id = data._id;
          this.title = data.title;
          this.username = data.username;
          this.password = data.password;
          this.description = data.description;
          this.URL = data.URL;
          this.expire_date = data.expire_date;
          this.category = data.category;
        })
    });
    this._categoriesService.getCategories().subscribe((cats) => {
      this.categories = cats;
    });
    this.getPolicies();
  }

  public editDataset(back) {
    this.isAwaiting = true;
    let data = {
      title : this.title,
      username : this.username,
      password : this.password,
      description : this.description,
      URL : this.URL,
      expire_date : this.expire_date,
      category : this.category
    };
    this._datasetDetailService.saveDetail(this.id, data).subscribe((reply) => {
      this.isAwaiting = false;
      if(back == 'back') this.goBack();
      this.msg = "Datensatz wurde gespeichert";
      setTimeout(() => {
        this.msg = "";
      }, 3000);
    })
  }

  public deleteDataset() {
    if(confirm('Are you sure you want to delete this?')) {
      this._datasetDetailService.removeDetail(this.id).subscribe((reply) => {
      })
    }
  }

  public getPolicies() {
    this._datasetDetailService.getPolicies().subscribe((policies) => {
      this.policies= policies;
    });
  }

  public getGeneratedPassword() {
    console.log(this.policy);
    this._datasetDetailService.generatePassword("594415c6c88043551955c807").subscribe((pass) => {
      this.password = pass.pass;
    })
  }

  public goBack() {
    this._location.back();
  }

}
