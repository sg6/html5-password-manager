import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-category-tree',
  templateUrl: './category-tree.component.html',
  styleUrls: ['./category-tree.component.scss'],
})
export class CategoryTreeComponent implements OnInit {

  @Input() categories: any;

  constructor() { }

  ngOnInit() {

  }

}
