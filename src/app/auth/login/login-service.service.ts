import { Injectable } from '@angular/core';
import { env } from '../../../../.env';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

import { Router } from '@angular/router';

@Injectable()
export class LoginService {

  constructor(private _http : Http, private router: Router) {}
er
  public auth(creds) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this._http
        .post(env.apiUrl + '/api/auth', creds, { headers : headers })
        .map((response:Response) => {
          return this.successfulAuth(response);
        });
  }

  private successfulAuth(response) {
    response = response.json();
    if(response.success === 200) {
      // window.location.reload();
      this.router.navigateByUrl('/dashboard');
    }
    return [];
  }

}