import { Injectable } from '@angular/core';
import { env } from '../../../../.env';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DatasetService {


  constructor(private _http : Http) {}

  public getItems(id) {
    return this._http.get(env.apiUrl + '/api/items?category=' + id)
        .map((response:Response) => response.json());
  }

  public getPassword(id) {
    return this._http.get(`${env.apiUrl}/api/items/${id}?value=password`)
        .map((response:Response) => response.json());
  }

}
