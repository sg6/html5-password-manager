import { Injectable } from '@angular/core';
import { env } from '../../../../.env';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Router } from '@angular/router';

@Injectable()
export class CategoryService {

  constructor(private _http : Http, private router: Router) {}

  public getCategories() {
    return this._http
        .get(env.apiUrl + '/api/categories')
        .map((response:Response) => response.json())
        .catch((response) => {
          return this.catch401Error(response);
        });
  }

  public saveCategory(category) {
    return this._http
      .post(env.apiUrl + '/api/categories', category)
      .map((response: Response) => response.json())

  }

  private catch401Error(response) {
    if(response.status === 401) {
      console.log('401 error');
      this.router.navigateByUrl('/login');
    }
    return [];
  }

}
