module.exports = function(grunt) {

    grunt.initConfig({
        watch: {
            express: {
                files:  [ 'server/**'],
                tasks:  [ 'express:dev' ],
                options: {
                    spawn: false
                }
            }
        },
        express: {
            dev: {
                options: {
                    script: './server.js'
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-express-server');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['express:dev', 'watch']);
};