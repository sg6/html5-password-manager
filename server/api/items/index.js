'use strict';

var express = require('express');
var controller = require('./item.controller.js');
var Item = require('./item.model.js');
// var auth = require('../../auth/auth.service');

var router = express.Router();


router.post('/', controller.create);
router.get('/', controller.index);
router.get('/:id', controller.findOne);
router.put('/:id', controller.update);
router.delete('/:id', controller.delete);

module.exports = router;
