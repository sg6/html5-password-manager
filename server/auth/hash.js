var bcrypt = require('bcrypt');

const hashedMasterKey = "$2a$12$CExphP3Zg0tGNyvLwuytfeX4w.SVw5MKA/f/5CHZWnoe8RqmQ2/GG";

exports.checkMasterKey = function (plaintext) {
  return bcrypt.compareSync(plaintext, hashedMasterKey);
}