'use strict';

var crypto = require('crypto');
var mongoose = require('../../db_connect.js');
var Schema = mongoose.Schema;

var CategorySchema = new Schema({
  name : String,
  parent : String,
  items : Array,
  attributes: Array
});


module.exports = mongoose.model('Category', CategorySchema);
