'use strict';

const mongoose = require('../../db_connect.js');

const Schema = mongoose.Schema;

let PoliciesSchema = new Schema({
  name : String,
  rules : Object
});


module.exports = mongoose.model('Policies', PoliciesSchema);
