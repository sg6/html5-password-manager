import { Component, OnInit } from '@angular/core';
import { CategoryService } from './categories.service';
import { ActivatedRoute, Params} from '@angular/router';


@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
  providers: [CategoryService]
})
export class CategoriesComponent implements OnInit {

  categories : any = [];
  activeCategory : String;
  showCreateField : Boolean = false;
  categoriesStructured : any = [];

  newTitle;
  newParentCategory;

  constructor(private _categoryService : CategoryService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.getCategories();

    this.activatedRoute.params.subscribe((params: Params) => {
        this.activeCategory = params['id'];
    });
  }

  public showCreate() {
    this.showCreateField = !this.showCreateField;
  }

  public getCategories() {
    this._categoryService.getCategories()
        .subscribe(resCategories => {
          this.categories = resCategories;
          this.categoriesStructured = (this.buildHierarchy(this.categories));
        } );
  }

  public create() {
    let parent = (this.newParentCategory !== 0) ? this.newParentCategory : null;
    let category: any = {
      name : this.newTitle,
    };
    if(this.newParentCategory !== 0) {
      category.parent = this.newParentCategory;
    }
    this._categoryService.saveCategory(category).subscribe(res => {
      this.getCategories();
    })
  }

  public buildHierarchy(arry) {

    const roots = [],
          children = {};

    // find the top level nodes and hash the children based on parent
    for (let i = 0, len = arry.length; i < len; ++i) {
      let item = arry[i],
        p = item.parent,
        target = !p ? roots : (children[p] || (children[p] = []));

      target.push(item);
    }

    // function to recursively build the tree
    let findChildren = function(parent) {
      if (children[parent._id]) {
        parent.children = children[parent._id];
        for (let i = 0, len = parent.children.length; i < len; ++i) {
          findChildren(parent.children[i]);
        }
      }
    };

    // enumerate through to handle the case where there are multiple roots
    for (let i = 0, len2 = roots.length; i < len2; ++i) {
      findChildren(roots[i]);
    }

    return roots;
  }


}
