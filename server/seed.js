const 
    Category = require('./api/categories/category.model'),
    Item = require('./api/items/item.model'),
    User = require('./api/users/user.model'),
    ObjectId = require('mongoose').Types.ObjectId;

if(process.argv[2] === "drop") {
    User.remove({});
    Item.remove({});
    Category.remove({}, function(err, res) {
        if(err) console.log('err', err);
        console.log(res);
    });
    console.log('removed');
}

var _users = [ 
    {
        username: "stefan",
        email : "stefan@sg6.eu",
        name : "Stefan",
        password: "password"
    },
    {
        username: "admin",
        email : "admin@sg6.eu",
        name : "Admin",
        password: "adminpass"
    }
];

var _categories = [
    {
        _id : ObjectId("5806216dbda04d43f84fd010"),
        name : "Intern",
        items :["5806216dbda04d43f84fd013", "5806216dbda04d43f84fd014"]
    },
    {
        _id : ObjectId("5806216dbda04d43f84fd011"),
        name : "Kunde Mayer",
        items : ["5806216dbda04d43f84fd015"]
    },
    {
        _id : ObjectId("5806216dbda04d43f84fd012"),
        name : "Mayer Website",
        parent : "5806216dbda04d43f84fd011",
        items : ["5806216dbda04d43f84fd016", "5806216dbda04d43f84fd017", "5806216dbda04d43f84fd018"]
    }
];

var _items = [
    {
        _id : ObjectId("5806216dbda04d43f84fd013"),
        title : "Wissensmanagement Login",
        username : "wissen",
        password : "wissenpass",
        description: "Passwort für wissen.intern",
        URL : "https://wissen.intern"
    },
    {
        _id : ObjectId("5806216dbda04d43f84fd014"),
        title : "Dokumentenverwaltung Login",
        username : "dokumenten",
        password : "dokupass",
        description: "Passwort für doku.intern",
        URL : "https://doku.intern"
    },
    {
        _id : ObjectId("5806216dbda04d43f84fd015"),
        title : "Mayer E-Mail",
        username : "office@mayer.com",
        password : "mailpw00",
        description: "Zugangsdaten für Mayer Office E-Mail",
        URL : "https://webmail.mayer.com"
    },
    {
        _id : ObjectId("5806216dbda04d43f84fd016"),
        title : "Mayer WordPress",
        username : "mayeruser",
        password : "wppass10",
        URL : "https://mayer.com/wp-admin"
    },
    {
        _id : ObjectId("5806216dbda04d43f84fd017"),
        title : "Mayer FTP",
        username : "ftpusr",
        password : "ftppassmayer",
        URL : "sftp://mayer.com"
    },
    {
        _id : ObjectId("5806216dbda04d43f84fd018"),
        title : "Mayer MySQL",
        username : "mayersql",
        password : "sql!pass"
    }
]

Category.insertMany(_categories);
Item.insertMany(_items);

for(let i = 0; i < _users.length; i++) {
    console.log('u', _users[i]);
    var U = new User(_users[i]);
    U.save(function(){})
}