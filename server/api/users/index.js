'use strict';

var express = require('express');
var controller = require('./user.controller.js');
var User = require('./user.model.js');

var router = express.Router();

router.post('/', controller.create);
router.get('/me', controller.me);
router.get('/', controller.index);
router.get('/:id', controller.findOne);
router.put('/:id', controller.update);

module.exports = router;

