'use strict';

var crypto = require('crypto');
var mongoose = require('../../db_connect.js');
var Schema = mongoose.Schema;

var ItemSchema = new Schema({
  title : String,
  username : String,
  password : String,
  description: String,
  URL : String,
  note: String,
  expire_date : Date,
  policy : Object,
  attributes: Array
});

module.exports = mongoose.model('Item', ItemSchema);
