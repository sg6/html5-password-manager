'use strict';

var express = require('express');

var controller = require('./tags.controller.js');

var Tags = require('./tags.model.js');

var router = express.Router();


router.post('/', controller.create);
router.get('/', controller.index);
router.get('/:id', controller.findOne);
router.put('/:id', controller.update);

module.exports = router;
